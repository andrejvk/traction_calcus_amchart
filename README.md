# webpack-empty-template
---
В этом репозитории показано, как построить пустое приложение на webpack. Основано на туториале с [официального сайта](https://webpack.js.org/guides/getting-started/).

npm start
npm run server
npm run build
npm run-script build

создается простая основа webpack.js.org
https://webpack.js.org/guides/development/

устанавливаем компоненты package.json

"devDependencies": {
    "css-loader": "^2.1.1",
    "express": "^4.17.1",
    "script-loader": "^0.7.2",
    "style-loader": "^0.23.1",
    "vue-loader": "^15.7.0",
    "vue-template-compiler": "^2.6.10",
    "webpack": "^4.29.6",
    "webpack-cli": "^3.3.0",
    "webpack-dev-middleware": "^3.7.0",
    "webpack-dev-server": "^3.2.1"
	
"dependencies": 
    "@amcharts/amcharts4": "^4.4.10",
    "clean-webpack-plugin": "^2.0.1",
    "flatpickr": "^4.5.7",
    "html-webpack-plugin": "^3.2.0",
    "lodash": "^4.17.11",
    "vue": "^2.6.10",
    "vue-flatpickr": "^2.3.0",
    "vue-flatpickr-component": "^8.1.2",
    "vuex": "^3.1.0"
	
в index.js импортируем 
	import _ from 'lodash';
	import Vue from '../node_modules/vue/dist/vue.js';
	import Vuex from 'vuex';
	import App from './calcus.vue'; компоненты

создаются файлы vue, scc :
	подключение стилей import './calcus.css' 
	компонент подключаем в своем проекте import App from './calcus.vue'  


в <template> </template> пишем компоненты для отрисоки графиков 

подключаем amcharts
	https://www.amcharts.com/docs/v4/getting-started/integrations/using-vue-js/
	
	import * as am4core from "@amcharts/amcharts4/core";
	
	import * as am4charts from "@amcharts/amcharts4/charts";
	
	import am4themes_animated from "@amcharts/amcharts4/themes/animated";
		
 
	метод : для переключения вида свойств и выбранной галочки 
		load_radioE_SPE
		load_radioST_IV

	метод : get_data_json
		подключаем файл с данными
		парсится JSON, записываем в переменную this.data_json
		выбираем номер перегона из всего набора данных 
		передаем номер на обработку в метод this.generationDataGraph(i);
	
	метод : generationDataGraph
		получаем номер перегона 
		генерируем данные для графика 1 и 2		
		записываем все что связано с этим перегоном в переменную this.data_jsonSelected = this.data_json[iN] ;
		обрабатываем данные для первого и второго графика :
			1)объединяем пункты в один по определенному критерию , по одной оси группировка
			2)группируем повторения выбираем минимальное значение
		
		вызываем метод this.grafik_blok(list,'1',iN); 
			передаем подготовленные данне для одного графика list ,  
			передаем идентификатор на какой график ссылаться '1'
			передаем номер перегона iN
		
	метод : grafik_blok
		основа постройки графика куда прикрепляем
		(list,'1',iN)
		получаем параметры tipGraf,NumChartdiv,iN
		смотрим какой NumChartdiv и распределяем прикрепляя amcharts к компоненту template где будем отрисовывать:
			chart1_1 = am4core.create(self.$refs['chartdiv1_1_'+iN][0], am4charts.XYChart);
			из tipGraf вытаскиваем значения по линиям q ,qmax,qmin
		
		вызываем метод this.Grafik(chart1_2,data1_2,'1');
			передаем amcharts куда прикрепляем (в chart1_2) , и какой тип графика am4charts.XYChart
			передаем массив объектов для линии data1_2 (данные)
			передаем передаем номер к какому графику относится '1'
	
	
	метод : Grafik	
		постройка графика
		получаем параметры chart,data,NumChartdiv
		chart.data = data; сохраняем данные 

		в зависимости на какой график ссылаемся NumChartdiv 
		выбираем какая подпись пооси x и что мы берем за ось x category

		здесь настраиваем что как и где отобразаем оси координат 
		добавляем легенду
		
		вызываем метод this.GrafikSeriesData1(chart,valueAxis,valueAxis2,NumChartdiv,categoryT);
			передаем параметры графика chart
			передаем параметры осей графика valueAxis,valueAxis2
			передаем номер графика на который ссылаемся NumChartdiv
			передаем тип категории 	(categoryT) которую берем за основу по оси x 
	
	метод : Grafik
		получаем параметры chart,valueAxis,valueAxis2,NumChartdiv,categoryT
		настройка сколько линий у графика
		в зависимости какой график обределяем ему индивидуальные параметры 
			var seriesName2 = "energy" ;
			var dataFieldsValueY = "energy";
			var dataFieldsCategoryX = categoryT;
			var am4coreColor = "#00FFFF";			
			var seriesName = "энергия";
			var seriesStrokeDasharray = 0;//4
			valueAxis.title.text = 'энергия (кВт*ч)';
			
		вызываем метод this.GrafikSeries(chart,dataFields_valueY,dataFields_categoryX,am4core_color,series_name,series_strokeDasharray,series_name2,valueAxis);
		вызываем метод this.GrafikSeries(chart,dataFields_valueY,dataFields_categoryX,am4core_color,series_name,series_strokeDasharray,series_name2,valueAxis2);
			передаем индивидуальные параметры 

	метод : GrafikSeries
		настройка графика , сама отрисовка
		настройка подписей при наведении на графике
		получаем параметры chart,dataFields_valueY,dataFields_categoryX,am4core_color,series_name,series_strokeDasharray,series_name2,valueAxis


	
 