import _ from 'lodash';
import Vue from '../node_modules/vue/dist/vue.js';
import Vuex from 'vuex';
import App from './calcus.vue';
Vue.use(Vuex);

function component() {
  //let element = document.createElement('div');
  //element.innerHTML = _.join(['Hello', 'webpack'], ' ');
  //element.innerHTML = `Hello webpack andrej`;
  //return element;
  

	let appDiv = document.createElement('div');
	appDiv.id = "app";
	document.body.appendChild(appDiv);


	var state = {};
	var mutations = {};
	var actions = {};

	// This is store!!!.
	var store = new Vuex.Store({
		state,
		mutations,
		actions
	})

	new Vue({
		el: '#app',
		store,
		template: `<App/>`,
		components: { App }
	})


	return appDiv;
}

document.body.appendChild(component());
