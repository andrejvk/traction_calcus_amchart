  const path = require('path');
  const HtmlWebpackPlugin = require('html-webpack-plugin');
  const CleanWebpackPlugin = require('clean-webpack-plugin');
  const VueLoaderPlugin = require('vue-loader/lib/plugin');
module.exports = {
	mode: 'development',
	entry: './src/index.js',	
	devtool: 'inline-source-map',
	devServer: {
		contentBase: './dist'		
	},
	output: {
		//filename: 'main.js',
		filename: '[name].js',
		path: path.resolve(__dirname, 'dist'),
		
		publicPath: ''
	},
	module: {
		rules: [
		  // ... другие правила
		  {
			test: /\.vue$/,
			loader: 'vue-loader'
		  },
		  {
			 test: /\.css$/,
			 use: [
			   'style-loader',
			   'css-loader'
			 ]
		  },
		  {
			test: /\.exec\.js$/,
			use: [ 'script-loader' ]
		  }
		]
	  },
	plugins: [
		// убедитесь что подключили плагин!
		new VueLoaderPlugin()
	]
};